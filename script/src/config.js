const path = require("path");

const RELEASES_REPO_URL =
  "https://gitlab.com/itential/customersuccess/releases.git";
const RELEASES_REPO_PATH = path.join(__dirname, "../../releases");
const VERSIONS_PATH = path.join(RELEASES_REPO_PATH, "./versions");

const INSTANCES_PATH = path.join(__dirname, "../../instances");

module.exports = {
  RELEASES_REPO_URL,
  RELEASES_REPO_PATH,
  VERSIONS_PATH,
  INSTANCES_PATH,
};
