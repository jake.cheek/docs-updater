const fs = require("fs");
const path = require("path");
const { VERSIONS_PATH } = require("./config");

const getAllBlueprints = () =>
  fs.readdirSync(VERSIONS_PATH).filter((file) => file.startsWith("blueprint"));

const getAllVersions = () => {
  const blueprints = getAllBlueprints();
  return blueprints.map((blueprint) =>
    blueprint.replace("blueprint-", "").replace(".json", "")
  );
};

const getBlueprintPackages = (version) => {
  const file = path.join(VERSIONS_PATH, `./blueprint-${version}.json`);
  const blueprint = JSON.parse(fs.readFileSync(file, "utf-8"));
  return Object.entries(blueprint.packages).map(
    ([key, value]) => `${key}@${value}`
  );
};

module.exports = { getAllBlueprints, getAllVersions, getBlueprintPackages };
