const prettier = require("prettier");

const renderMarkdown = (iapData) => {
  const markdown = iapData
    .map((appData) => renderAppMarkdown(appData))
    .join("\n<br>\n\n");

  return prettier.format(markdown, { parser: "markdown" });
};

const renderAppMarkdown = (appData) => {
  const { name, methods, views } = appData;

  let markdown = "";

  markdown += `#### ${formatName(name)}\n\n`;
  if (methods.length) {
    markdown += renderMethodsTable(methods);
    if (views.length) {
      markdown += "\n\n<br>\n\n";
    } else {
      markdown += "\n";
    }
  }
  if (views.length) {
    markdown += renderViewsTable(views) + "\n";
  }

  return markdown;
};

const formatName = (name) => {
  let editedName = name
    .split(/(?=[A-Z])/)
    .join(" ")
    .replace("Service Cog", "Service Manager")
    .replace("Work Flow", "Workflow");

  const abbreviations = ["AG", "JSON", "JST", "MOP", "NSO"];
  abbreviations.forEach((abbreviation) => {
    editedName = editedName
      .replace([...abbreviation].join(" "), abbreviation)
      .replace(
        abbreviation.charAt(0).toUpperCase() +
          abbreviation.slice(1).toLowerCase(),
        abbreviation
      );
  });

  return editedName;
};

const renderMethodsTable = (methods) => {
  const methodsRoles = [
    ...new Set(methods.flatMap((method) => method.roles)),
  ].sort();

  const headers = [
    { label: "API Method/Task", align: "left", key: "name", sort: "ascending" },
    ...methodsRoles.map((role) => ({
      label: role,
      align: "center",
      key: (row) => (row.roles.includes(role) ? "x" : ""),
    })),
  ];

  return renderMarkdownTable(headers, methods);
};

const renderViewsTable = (views) => {
  const viewsRoles = [...new Set(views.flatMap((view) => view.roles))].sort();

  const headers = [
    {
      label: "",
      align: "left",
      key: "name",
      sort: { order: "ascending", priority: 0 },
    },
    {
      label: "view",
      align: "left",
      key: "path",
      sort: { order: "ascending", priority: 1 },
    },
    ...viewsRoles.map((role) => ({
      label: role,
      align: "center",
      key: (row) => (row.roles.includes(role) ? "x" : ""),
    })),
  ];

  return renderMarkdownTable(headers, views);
};

const compareAlphabetical = (a, b) => {
  return a.localeCompare(b, undefined, { sensitivity: "base", numeric: true });
};

/*
 data: {
   headers: [
     { label: '', align: 'left', key: 'name', sort: { order: 'ascending', priority: 0 } },
     { label: 'view', align: 'left', key: 'path', sort: { order: 'descending', priority: 1 } },
     { label: 'admin', align: 'center', key: (row) => row.roles.includes('admin') ? 'x' : '' }
   ],
 }
 */
const renderMarkdownTable = (headers, data) => {
  const headerRows = [
    headers.map((header) => header.label),
    headers.map((header) => {
      switch (header.align) {
        case "left":
          return ":--";
        case "center":
          return ":-:";
        case "right":
          return "--:";
        default:
          return "---";
      }
    }),
  ];

  const compare = makeCompareFunction(headers);
  const dataRows = data
    .sort((a, b) => compare(a, b))
    .map((row) => headers.map((header) => applyKey(row, header.key)));

  return [...headerRows, ...dataRows]
    .map((row) => renderMarkdownRow(row))
    .join("\n");
};

const makeCompareFunction = (headers) => {
  const getSortPriority = (header) => header.sort.priority || 0;
  const getSortOrderCoefficient = (header) => {
    const sortOrder = header.sort.order || header.sort;
    if (sortOrder === "ascending") {
      return 1;
    } else if (sortOrder === "descending") {
      return -1;
    } else {
      throw Error("header sort order must be one of [ascending, descending]");
    }
  };

  const sortHeaders = headers
    .filter((header) => header.sort)
    .sort((a, b) => getSortPriority(a) - getSortPriority(b));

  return function compare(a, b) {
    for (const sortHeader of sortHeaders) {
      const aValue = applyKey(a, sortHeader.key);
      const bValue = applyKey(b, sortHeader.key);
      const comparison =
        getSortOrderCoefficient(sortHeader) *
        compareAlphabetical(aValue, bValue);

      if (comparison !== 0) return comparison;
    }
    return 0;
  };
};

const applyKey = (row, key) => {
  switch (typeof key) {
    case "string":
      return row[key];
    case "function":
      return key(row);
    default:
      throw Error("header.key must be a string or function");
  }
};

const renderMarkdownRow = (cells) => {
  return "| " + cells.join(" | ") + " |";
};

module.exports = { renderMarkdown };
