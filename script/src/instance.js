const child_process = require("child_process");
const fs = require("fs");
const path = require("path");

const installPackage = (package, path) => {
  if (!fs.existsSync(path)) fs.mkdirSync(path, { recursive: true });
  return child_process.execSync(
    `cd "${path}" && npm i --legacy-peer-deps ${package}`
  );
};

const getPronghorns = (path) =>
  getFiles(path).filter((file) => file.endsWith("pronghorn.json"));

const getFiles = (dir) => {
  const dirents = fs.readdirSync(dir, { withFileTypes: true });
  const files = dirents.map((dirent) => {
    const res = path.resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : res;
  });
  return Array.prototype.concat(...files);
};

const processPronghorn = (path) => {
  const pronghornContents = fs.readFileSync(path);
  const pronghorn = JSON.parse(pronghornContents);

  return {
    name: pronghorn.export,
    methods: (pronghorn.methods || []).map((method) => ({
      name: method.name || "",
      roles: method.roles || [],
    })),
    views: (pronghorn.views || []).map((view) => ({
      name: view.title || "",
      path: view.path,
      roles: view.roles || [],
    })),
  };
};

module.exports = { installPackage, getPronghorns, processPronghorn };
