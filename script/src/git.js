const child_process = require("child_process");

const cloneRepo = (fromUrl, toPath) =>
  child_process.execSync(`git clone "${fromUrl}" "${toPath}"`);

const pullLatest = (path) => child_process.execSync(`cd "${path}" && git pull`);

module.exports = { cloneRepo, pullLatest };
