const {
  RELEASES_REPO_PATH,
  RELEASES_REPO_URL,
  INSTANCES_PATH,
} = require("./config");
const { cloneRepo, pullLatest } = require("./git");
const fs = require("fs");
const path = require("path");
const inquirer = require("inquirer");
const { getAllVersions, getBlueprintPackages } = require("./blueprints");
const {
  installPackages,
  installPackage,
  getPronghorns,
  processPronghorn,
} = require("./instance");
const { Progress } = require("clui");
const { renderMarkdown } = require("./markdown");

const main = async () => {
  console.log("Getting latest blueprints...");
  if (!fs.existsSync(RELEASES_REPO_PATH)) {
    cloneRepo(RELEASES_REPO_URL, RELEASES_REPO_PATH);
  } else {
    pullLatest(RELEASES_REPO_PATH);
  }

  const { version, moduleType } = await inquirer.prompt([
    {
      type: "list",
      name: "version",
      message: "Which version?",
      choices: getAllVersions().reverse(),
    },
    {
      type: "list",
      name: "moduleType",
      message: "Which module type(s)?",
      choices: ["Apps", "Adapters", "Apps & Adapters", "All"],
    },
  ]);

  const packages = getBlueprintPackages(version);
  const installPath = path.join(INSTANCES_PATH, version);

  for (const [i, package] of packages.entries()) {
    console.log(
      `(${i + 1}/${packages.length}) Installing ${package.slice(
        0,
        package.lastIndexOf("@")
      )}`
    );
    installPackage(package, installPath);
  }

  const pronghorns = getPronghorns(installPath)
    .filter((file) => {
      if (moduleType === "Apps") return file.includes("app-");
      if (moduleType === "Adapters") return file.includes("adapter-");
      if (moduleType === "Apps & Adapters")
        return file.includes("app-") || file.includes("adapter-");
      return true;
    })
    .map((file) => processPronghorn(file));

  const markdown = renderMarkdown(pronghorns);
  const markdownPath = path.join(
    installPath,
    `permissions-${moduleType.toLowerCase().replace(" ", "_")}.md`
  );
  fs.writeFileSync(markdownPath, markdown, "utf-8");
  console.log(`Permissions tables written to ${markdownPath}`);
};

main();
