# Docs Updater

This script generates permissions tables for documentation purposes.

## How To Use

```bash
cd script
npm i
npm start
```

Select an IAP version and which module types to generate permissions for (apps, adapters, etc.)

The script will install all dependencies for the selected IAP version, extract permission from their `pronghorn.json`s, then output a markdown file in the instances directory for the selected version (e.g. `instances/2021.1.2/permissions-apps.md`)
